import React, { useState } from "react";

import { Container } from "./styles";

interface BoxProps {
  values: string[];
}

// Challenge:
/**
 * Try to send some string through the "values" array
 * and then iterate over them in the Box below!
 *
 * Tip: you can use values.map function to iterate over
 * each value of the array!
 */

export function Box({ values }: BoxProps) {
  const [title, setTitle] = useState("banana");

  function handleOnClick(value: string) {
    setTitle(value);
  }

  return (
    <Container>
      <p>{title}</p>

      <button type="button" onClick={() => handleOnClick("apple")}>
        Click me!
      </button>

      {values.map((value) => (
        <p>{value}</p>
      ))}
    </Container>
  );
}

import React from "react";
import { Box } from "./components/Box/Box";

const values = ["value 1", "value 2", "value 3", "value 4", "value 5"];

function App() {
  return (
    <>
      <ul>
        <li>Item 10</li>
        <li>Item 21</li>
        <li>Item 32</li>
        <li>Item 43</li>
      </ul>

      <Box values={values} />
    </>
  );
}

export default App;
